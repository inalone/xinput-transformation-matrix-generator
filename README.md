# xinput transformation matrix generator

**Created for Python 3.7**

To find out device name, type in "xinput list"

If you do not have xinput, install it from your distro's repo (e.g. xorg-xinput for arch)

**Usage: **

python3 transformationMatrix.py

and then (do not include the [])

xinput set-prop "[**device name**]" --type=float "Coordinate Transformation Matrix" [ **script values**]"
